import React from "react";
import ShareIcon from "@material-ui/icons/Share";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import { Box, Typography, IconButton } from "@material-ui/core";

const Actions = ({ setFavorite, isFavoriteItem, setOpenModal }) => {
  return (
    <Box style={{ display: "flex" }}>
      <IconButton onClick={setFavorite}>
        {isFavoriteItem ? <FavoriteIcon /> : <FavoriteBorderIcon />}
      </IconButton>

      <IconButton
        onClick={() => {
          setOpenModal(true);
        }}
      >
        <ShareIcon />
      </IconButton>
    </Box>
  );
};

export default Actions;
