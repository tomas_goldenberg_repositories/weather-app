import React, { useState } from "react";
import {
  Modal,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import "../styles/components/ShareLinkModal.scss";

const ShareLinkModal = ({ openModal, setOpenModal }) => {
  const [openNotification, setOpenNotification] = useState(false);

  const handleClose = () => {
    setOpenModal(false);
  };

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotification(false);
  };

  const copyText = () => {
    var copyText = document.getElementById("link");

    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    setOpenNotification(true);

    setTimeout(() => {
      setOpenModal();
    }, 3000);
  };

  return (
    <div>
      <Modal
        open={openModal}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className="modal">
          <TextField
            label="Link of the result"
            fullWidth
            variant="outlined"
            value={window.location.href}
            id="link"
            InputProps={
              window.location.href && {
                endAdornment: (
                  <InputAdornment position="end">
                    {" "}
                    <IconButton
                      onClick={() => {
                        copyText();
                      }}
                    >
                      <FileCopyIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }
            }
          />

          <Snackbar
            open={openNotification}
            autoHideDuration={3000}
            style={{ position: "absolute", top: "150px" }}
            onClose={handleCloseNotification}
          >
            <SnackbarContent
              className="notification"
              message={"Link copied , ready to share !"}
            />
          </Snackbar>
        </div>
      </Modal>
    </div>
  );
};

export default ShareLinkModal;
