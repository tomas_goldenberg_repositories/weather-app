import React, { useState } from "react";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

import { Box, Typography, IconButton } from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import "../styles/components/Header.scss";
import ShareLinkModal from "./ShareLinkModal";
import Actions from "./Actions";

const Header = ({ isFavoritePage, isFavoriteItem, setFavorite }) => {
  const navigate = useNavigate();
  const [openModal, setOpenModal] = useState(false);

  const navigateHome = () => {
    navigate("/");
  };
  return (
    <Box className="header">
      <IconButton>
        <ArrowBackIcon onClick={navigateHome} />
      </IconButton>
      <Box>{isFavoritePage && <Typography>MY FAVORITES</Typography>}</Box>

      <Box className="headerActions">
        {!isFavoritePage && (
          <Actions
            setFavorite={setFavorite}
            isFavoriteItem={isFavoriteItem}
            setOpenModal={setOpenModal}
          />
        )}
      </Box>
      {openModal && (
        <Box className="modalContainer">
          <ShareLinkModal setOpenModal={setOpenModal} openModal={openModal} />
        </Box>
      )}
    </Box>
  );
};

export default Header;
