import React, { useState, useEffect } from "react";
import { TextField, Box, Button, Card } from "@material-ui/core";
import { useNavigate, useLocation } from "react-router-dom";
import "../styles/containers/Searcher.scss";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";

const Searcher = () => {
  const { getData } = require("country-list");
  const countries = getData();
  const location = useLocation();
  const navigate = useNavigate();
  const [selectedLocation, setSelectedLocation] = useState({
    city: "",
    country: {},
  });
  const [openNotification, setOpenNotification] = useState(false);

  const handleCloseNotification = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenNotification(false);
  };
  const handleSearch = () => {
    navigate(
      `weather/${selectedLocation.city}/${selectedLocation.country.code}`
    );
  };

  const goToFavorites = () => {
    navigate(`/favorites`);
  };

  useEffect(() => {
    if (location.state?.error) {
      setOpenNotification(true);
    }
  }, [location]);

  return (
    <Box className="searchedRoot">
      <Card className="searcherCard">
        <h1 className="searcherTitle">Weather Finder</h1>
        <TextField
          fullWidth
          label="City"
          className="searcherInput"
          placeholder="City"
          variant="outlined"
          onChange={(e) => {
            setSelectedLocation({
              ...selectedLocation,
              city: e.target.value,
            });
          }}
          value={selectedLocation.city}
        />
        <Autocomplete
          options={countries}
          fullWidth
          className="searcherInput"
          getOptionLabel={(option) => option.name}
          closeIcon={false}
          onChange={(e, value, name, instance) => {
            if (!instance) {
              return;
            }

            setSelectedLocation({
              ...selectedLocation,
              country: instance.option,
            });
          }}
          value={selectedLocation.country}
          renderInput={(params) => (
            <TextField {...params} variant="outlined" label="Countries" />
          )}
        />

        <Box className="searcherButtons">
          <Button
            color="primary"
            className="searcherButtom"
            variant="contained"
            onClick={handleSearch}
            disabled={!selectedLocation.city || !selectedLocation.country?.name}
          >
            Search
          </Button>
          <Button
            className="searcherButtom"
            onClick={() => {
              goToFavorites();
            }}
          >
            Go to Favorites
          </Button>
        </Box>
        <Snackbar
          open={openNotification}
          autoHideDuration={3000}
          className="errorNotification"
          onClose={handleCloseNotification}
        >
          <SnackbarContent
            style={{ backgroundColor: "red" }}
            message={"City not found"}
          />
        </Snackbar>
      </Card>
    </Box>
  );
};

export default Searcher;
