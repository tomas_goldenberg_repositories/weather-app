import React, { useState, useEffect } from "react";
import { Box, Typography } from "@material-ui/core";
import { useParams, useNavigate } from "react-router-dom";
import "../styles/containers/Result.scss";
import { getWeather } from "../api/weather";
import Header from "../components/Header";
import { useSelector, useDispatch } from "react-redux";
import { setFavorites } from "../store/actions/weatherActions";
import Actions from "../components/Actions";
import ShareLinkModal from "../components/ShareLinkModal";

const Result = ({ isItem, weather }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isFavorite, setIsFavorite] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const favorites = useSelector((state) => state.WeatherReducer.favorites);
  const { getName } = require("country-list");
  const [selectedWeather, setSelectedWeather] = useState({});
  const { city, country } = useParams();

  useEffect(() => {
    const getCity = async () => {
      try {
        const { data } = await getWeather(city, country);
        setSelectedWeather({
          id: data.id,
          city: data.name,
          country: getName(country),
          humidity: data.main?.humidity,
          temperature: data.main?.temp,
        });
      } catch (error) {
        navigate("/", { state: { error } });
      }
    };
    if (isItem) {
      setSelectedWeather(weather);
    } else {
      getCity();
    }
  }, [city, country, weather]);

  const handleFavorite = () => {
    const favoritesIds = favorites.map((city) => city.id);
    const isFavorite = favoritesIds.includes(selectedWeather.id);
    setIsFavorite(isFavorite);
  };

  const setFavorite = () => {
    if (isFavorite) {
      const nonRemovedFavorites = favorites.filter(
        (city) => city.id !== selectedWeather.id
      );
      dispatch(setFavorites(nonRemovedFavorites));
    } else {
      const newFavorites = [...favorites, selectedWeather];
      dispatch(setFavorites(newFavorites));
    }
  };

  useEffect(() => {
    handleFavorite();
  }, [favorites, selectedWeather]);
  return (
    <Box className="resultRoot">
      {!isItem && (
        <Header
          isFavoritePage={false}
          setFavorite={setFavorite}
          isFavoriteItem={isFavorite}
          s
        />
      )}

      <Box className={!isItem ? "resultsContainer" : "resultsItem"}>
        <Box className={isItem ? "itemLocationBox" : ""}>
          <Box>
            <Typography className="locationText">LOCATION</Typography>
            <h1 className="locationValue">{`${selectedWeather.city || ""}, ${
              selectedWeather.country || ""
            }`}</h1>
          </Box>

          {isItem && (
            <Actions
              setFavorite={setFavorite}
              isFavoriteItem={true}
              setOpenModal={setOpenModal}
            />
          )}
        </Box>

        <Box className="detailContainer">
          <Box className="detailBox">
            <Typography className="detailText">TEMP</Typography>
            <h1 className="detailValue">
              {selectedWeather.temperature || ""} C
            </h1>
          </Box>

          <Box>
            <Typography className="detailText">HUMITITY</Typography>
            <h1 className="detailValue">{selectedWeather.humidity || ""} %</h1>
          </Box>
        </Box>
      </Box>

      {openModal && (
        <Box className="resultModalContainer">
          <ShareLinkModal openModal={openModal} setOpenModal={setOpenModal} />
        </Box>
      )}
    </Box>
  );
};

export default Result;
