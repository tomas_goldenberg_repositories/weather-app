import React from "react";
import Header from "../components/Header";
import Result from "./Result";
import { useSelector } from "react-redux";
import { Box, Divider } from "@material-ui/core";

const MyFavorites = () => {
  const favorites = useSelector((state) => state.WeatherReducer.favorites);

  return (
    <Box>
      <Header isFavoritePage />
      {favorites.map((favWeather) => (
        <Box key={favWeather.id}>
          <Result isItem weather={favWeather} />
          <Divider className="divider" style={{ height: "5px !important" }} />
        </Box>
      ))}
    </Box>
  );
};

export default MyFavorites;
