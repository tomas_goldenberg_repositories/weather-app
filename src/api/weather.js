import { API_PATH, API_KEY } from "../config";
import axios from "axios";

const getWeather = async (city, country) => {
  return await axios.get(
    `${API_PATH}weather?q=${city},${country}&appid=${API_KEY}&units=metric`
  );
};

export { getWeather };
