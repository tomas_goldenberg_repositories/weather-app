import { SET_FAVORITES } from "../types";

export const setFavorites = (favorites) => (dispatch) => {
  dispatch({
    type: SET_FAVORITES,
    payload: {
      favorites,
    },
  });
};
