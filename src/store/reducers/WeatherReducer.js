import * as actionTypes from "../types";

const initialState = {
  favorites: [],
};

export const WeatherReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_FAVORITES:
      return {
        ...state,
        favorites: action.payload.favorites,
      };
    default:
      return state;
  }
};
