import React from "react";
import Searcher from "../containers/Searcher";
import Result from "../containers/Result";
import MyFavorites from "../containers/MyFavorites";
import { useForkRef } from "@material-ui/core";

const routes = [
  {
    path: "/",
    element: <Searcher />,
  },
  {
    path: "weather",
    children: [
      {
        path: "/:city/:country",
        element: <Result />,
      },
    ],
  },
  {
    path: "/favorites",
    element: <MyFavorites />,
  },
];

export default routes;
